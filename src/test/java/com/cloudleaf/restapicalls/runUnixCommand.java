package com.cloudleaf.restapicalls;

import java.io.File;
import java.io.IOException;

public class runUnixCommand {

	public static void main(String[] args) throws IOException {
		//String cmd = "rm -rf "+System.getProperty("user.dir")+"\\ProdData\\*.*";
		//String cmd = "ls ";
		//Runtime.getRuntime().exec(cmd);



		File file = new File(System.getProperty("user.dir")+"\\ProdData");      
		String[] myFiles;    
		if(file.isDirectory()){
			myFiles = file.list();
			System.out.println("Number of Files in the Prod Data Directory: "+ myFiles.length);
			
			for (int i=0; i<myFiles.length; i++) {
				File myFile = new File(file, myFiles[i]); 
				myFile.delete();
			}
		}
		
		System.out.println("All Files in the ProdData are Deleted");
		
	



	}

}
