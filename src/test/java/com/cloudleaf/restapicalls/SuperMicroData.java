package com.cloudleaf.restapicalls;


import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class SuperMicroData {

	static String tenantId = null;
	static String token = null;
	static final String USER = "SuperMicro";
	static final String PASSWORD = "Y0vkxSoixu789)";
	//static final String TENANTID = "3e05b577-fe0f-479b-bc1f-9fcf96327480";
	public String clusterName = null;
	@BeforeSuite
	public void suiteSetup()
	{
		clusterName = System.getProperty("clusterName");
	}


	//public static void main(String[] args) throws FileNotFoundException
	@Test
	public void superMicroData() 
	{
		String currentDir = System.getProperty("user.dir");
		System.out.println("Current dir using System:" +currentDir);

		String token1=null;
		//token1=Utils.getToken(USER,PASSWORD,clusterName);
		token1=Utils.getToken(USER,Utils.get(PASSWORD),clusterName);

		//Gives the Tenant Name and Total number of Sites in the Tenant
		tenantId=Utils.getSites(token1,clusterName);

		//Gives the Total number of Users  and Total number of Provisioned Gateways in the Tenant
		Utils.getUsers(token1,tenantId,clusterName);

		//Gives the Total number of Sensors,Active and Inactive sensors in the Tenant
		Utils.getSensors(token1, tenantId,clusterName);

		//Gives the Total number of Asse,clusterNamet Categories in the Tenant
		Utils.getAssetCategories(token1,clusterName);

		Utils.getProvisionedReceivers(token1,clusterName);

		//Utils.getProvisionedReceivers(token1,clusterName,"1.1.0","1528357765");

		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(currentDir+"\\ProdData\\SuperMicroData.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		for(Object obj:Utils.customerDataList)
		{
			out.println(obj);
		}

		out.close();

		//Clearing the array list
		Utils.customerDataList.clear();

	}



}
