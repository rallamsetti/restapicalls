package com.cloudleaf.restapicalls;


import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MagnaData {

	static String tenantId = null;
	static final String USER = "CloudleafMagnaAdmin";
	static final String PASSWORD = "IRLSgmtgGjsot)789)";
	//static final String TENANTID= "4fc97007-4fa7-46a6-b0d3-7146585355f1";
	
	String clusterName = null;
	@BeforeClass
	public void classSetup()
	{
		clusterName = System.getProperty("clusterName");
	}

	//public static void main(String[] args) throws FileNotFoundException
	@Test
	public void magnaData() 
	{

		String currentDir = System.getProperty("user.dir");
	    System.out.println("Current dir using System:" +currentDir);
	    
		String token1=null;
		//token1=Utils.getToken(USER,PASSWORD,clusterName);
		token1=Utils.getToken(USER,Utils.get(PASSWORD),clusterName);
		
		//Gives the Tenant Name and Total number of Sites in the Tenant
		tenantId=Utils.getSites(token1,clusterName);

		//Gives the Total number of Users  and Total number of Provisioned Gateways in the Tenant
		Utils.getUsers(token1,tenantId,clusterName);

		//Gives the Total number of Sensors,Active and Inactive sensors in the Tenant
		Utils.getSensors(token1, tenantId,clusterName);

	//Gives the Total number of Asse,clusterNamet Categories in the Tenant
		Utils.getAssetCategories(token1,clusterName);
		
		//Utils.getProvisionedReceivers(token1,clusterName);
		
		Utils.getProvisionedReceivers(token1,clusterName,"1.1.0","1528357765");

		//Utils.getAssets(token1, TENANTID);

		//Create file MagnaData.txt file in D Drive
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(currentDir+"\\ProdData\\MagnaData.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		//Write the Customer Data which is stored in the ArrayList to the above file
		for(Object obj:Utils.customerDataList)
		{
			out.println(obj);
		}

		out.close();

		//Clearing the array list
		Utils.customerDataList.clear();

	}



}
