package com.cloudleaf.restapicalls;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class ProdDataFromXl {

	String tenantId = null;
	
	@BeforeSuite
	public void removeAllFiles() throws IOException
	{
		System.out.println("Before Suite is executed first and it will remove all the file in ProdData Folder");
		File file = new File(System.getProperty("user.dir")+"\\ProdData");      
		String[] myFiles = null;    
		if(file.isDirectory()){
			myFiles = file.list();
			System.out.println("Number of Files in the Prod Data Directory: "+ myFiles.length);
			
			for (int i=0; i<myFiles.length; i++) {
				File myFile = new File(file, myFiles[i]); 
				myFile.delete();
			}
		}
		
		System.out.println("All Files in the ProdData are Deleted and number of files in ProdData Folder are : " );
	}

	@DataProvider
	public Object[][] readData() throws IOException, InvalidFormatException
	{
		File inputxl = new File("src//test//resources//TestData//ProdTenantInfo.xlsx");
		FileInputStream fis = new FileInputStream(inputxl);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		XSSFSheet ws =  workbook.getSheetAt(0);

		XSSFRow rowsno = ws.getRow(0);
		int colNum = rowsno.getLastCellNum();
		System.out.println("Total Number of Columns in the excel is : "+colNum);
		int rowNum = ws.getLastRowNum()+1;
		System.out.println("Total Number of Rows in the excel is : "+rowNum);

		Object[][] data = new Object[rowNum][colNum];

		for(int row=0; row<rowNum;row++)
		{
			for(int column=0;column<ws.getRow(row).getLastCellNum();column++)
			{
				data[row][column] = ws.getRow(row).getCell(column).getStringCellValue();		
			}	
		}

		return data;
	}


	@Test(dataProvider="readData")
	public void cloudleafFlow(String userId,String password,String clusterName,String fileName)
	{

		System.out.println(userId+"------"+password+"-------"+clusterName+"------"+fileName);
		String currentDir = System.getProperty("user.dir");
		System.out.println("Current dir using System:" +currentDir);

		String token1=null;
		//token1=Utils.getToken(USER,PASSWORD,clusterName);
		token1=Utils.getToken(userId,Utils.get(password),clusterName);

		//Gives the Tenant Name and Total number of Sites in the Tenant
		tenantId=Utils.getSites(token1,clusterName);

		//Gives the Total number of Users  and Total number of Provisioned Gateways in the Tenant
		Utils.getUsers(token1,tenantId,clusterName);

		//Gives the Total number of Sensors,Active and Inactive sensors in the Tenant
		Utils.getSensors(token1, tenantId,clusterName);

		//Gives the Total number of Asse,clusterNamet Categories in the Tenant
		Utils.getAssetCategories(token1,clusterName);

		//Utils.getProvisionedReceivers(token1,clusterName);

		Utils.getProvisionedReceivers(token1,clusterName,"1.1.0","1528357765");

		//Create file OrangeData.txt file in D Drive
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(currentDir+"\\ProdData\\"+fileName+".txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		//Write the Customer Data which is stored in the ArrayList to the above file
		for(Object obj:Utils.customerDataList)
		{
			out.println(obj);
		}

		out.close();

		//Clearing the array list
		Utils.customerDataList.clear();

	}


}

