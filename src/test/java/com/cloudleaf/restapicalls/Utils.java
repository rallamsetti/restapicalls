package com.cloudleaf.restapicalls;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Utils {


	static String token = null;
	static List<Object>  customerDataList = new ArrayList<Object>();

	static char[] chars = {
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
			'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
			'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
			'y', 'z', '0', '1', '2', '3', '4', '5',
			'6', '7', '8', '9', 'A', 'B', 'C', 'D',
			'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
			'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
			'U', 'V', 'W', 'X', 'Y', 'Z', '!', '@',
			'#', '$', '%', '^', '&', '(', ')', '+',
			'-', '*', '/', '[', ']', '{', '}', '=',
			'<', '>', '?', '_', '"', '.', ',', ' '
	};



	public static String getToken(String userName,String password,String clusterName )
	{
		String getTokenUrl = "https://"+clusterName+ ".cloudleaf.io/cloudos/api/1/user/login";
		System.out.println(getTokenUrl);
		try
		{
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			HttpPost postRequest = new HttpPost(getTokenUrl);
			StringEntity input = new StringEntity("{\"login\":\"" + userName + "\",\"secret\":\"" + password +"\"}");
			input.setContentType("application/json");
			postRequest.setEntity(input);
			HttpResponse result = httpClient.execute(postRequest);
			String json = EntityUtils.toString(result.getEntity(), "UTF-8");
			//System.out.println(result.getStatusLine());
			JSONParser parser = new JSONParser();
			Object resultObject = parser.parse(json);

			JSONObject obj = (JSONObject) resultObject;
			token = (String) obj.get("token");
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("TOKEN: "+token);

		return token;
	}

	public static void getAssets(String token,String tenantId,String clusterName)

	{
		String getAssetsUrl = "https://"+clusterName+ ".cloudleaf.io/cloudos/api/1/assets?tenantId="+tenantId;

		try{
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			HttpGet getRequest = new HttpGet(getAssetsUrl);
			getRequest.addHeader("content-type", "application/json");
			getRequest.addHeader("token", token);
			HttpResponse result = httpClient.execute(getRequest);
			String json = EntityUtils.toString(result.getEntity(), "UTF-8");


			System.out.println(result.getStatusLine()+"\n "+json);

			/* Sample Response
			 * 
			 * [
  {
    "id": "3290ddb3-e36b-4a37-841c-b4297e9c8e3f",
    "externalId": "ASST B2B",
    "name": "ASSTName B2B",
    "tenantId": "5f41f209-e8fc-487e-87df-08bf6f01d73b",
    "status": "MONITORED",
    "sortOrder": 0,
    "createdAt": 1527847066957,
    "modifiedAt": 1528722960040,
    "categoryName": "Pallets",
    "category": "11bdd050-1751-4ab6-aa24-1ebfc6b0b386"
  },
  {
    "id": "a0797637-43e5-47b6-b4da-a2970a014519",
    "externalId": "1bab",
    "name": "1bab",
    "tenantId": "5f41f209-e8fc-487e-87df-08bf6f01d73b",
    "status": "MONITORED",
    "sortOrder": 0,
    "createdAt": 1528708285309,
    "modifiedAt": 1528708286235,
    "categoryName": "Pallets12",
    "category": "11bdd050-1751-4ab6-aa24-1ebfc6b0b386"
  },	*/
			JSONParser parser = new JSONParser();
			Object resultObject = parser.parse(json);
			JSONArray array = (JSONArray) resultObject;
			int totalAssets = array.size();
			System.out.println("TOTAL NUMBER OF ASSETS: "+totalAssets);

			customerDataList.add("TOTAL ASSETS: "+totalAssets);
			int nullassets = 0;
			StringBuilder sb = new StringBuilder();
			for (Object elem: array) 
			{
				JSONObject obj = (JSONObject)elem;
				String assetname = (String) obj.get("name");
				String assetid = (String) obj.get("externalId");
				if(assetname==null || assetname.isEmpty()){
					//System.out.println("ASSET NAME IS not available for ID :: "+assetid);
					sb.append("ASSET NAME IS not available for ID :: "+assetid+"\r\n");
					nullassets++;
				}

			}
			System.out.println(sb.toString());
			customerDataList.add("ASSETS WITHOUT NAME \r\n "+sb.toString());

			System.out.println("TOTAL NUMBER OF ASSET WITH ASSET NAME AS NULL : "+ nullassets);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void getUsers(String token,String tenantId,String clusterName)
	{

		String getUsersUrl = "https://"+clusterName+ ".cloudleaf.io/cloudos/api/1/users?tenantId="+tenantId;

		try{
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();

			HttpGet getRequest = new HttpGet(getUsersUrl);

			getRequest.addHeader("content-type", "application/json");
			getRequest.addHeader("token", token);

			HttpResponse result = httpClient.execute(getRequest);

			String json = EntityUtils.toString(result.getEntity(), "UTF-8");
			System.out.println(result.getStatusLine()+"\n "+json);

			JSONParser parser = new JSONParser();
			Object resultObject = parser.parse(json);

			JSONArray array = (JSONArray) resultObject;
			System.out.println("TOTAL NUMBER OF USERS: "+array.size());

			int normalUsers = 0;
			for (Object elem: array) 
			{
				JSONObject obj = (JSONObject)elem;
				String id = (String) obj.get("id");
				String name = (String) obj.get("name");
				String api_usertype= (String) obj.get("type");
				if(api_usertype.equalsIgnoreCase("NORMAL_USER"))
				{
					normalUsers ++;
				}


				System.out.println("Name :: "+name +", id :: "+id);
			}


			System.out.println("TOTAL NUMBER OF NORMAL USERS: "+ normalUsers);

			customerDataList.add("NORMAL USERS: "+normalUsers);


		} catch (Exception e) {
			e.printStackTrace();
		}


	}

	public static void getSensors(String token,String tenantId,String clusterName)
	{

		String getUsersUrl = "https://"+clusterName+ ".cloudleaf.io/cloudos/api/1/sensortags?tenantId="+tenantId;

		try{
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			HttpGet getRequest = new HttpGet(getUsersUrl);
			getRequest.addHeader("content-type", "application/json");
			getRequest.addHeader("token", token);
			HttpResponse result = httpClient.execute(getRequest);
			String json = EntityUtils.toString(result.getEntity(), "UTF-8");
			System.out.println(result.getStatusLine()+"\n "+json);
			JSONParser parser = new JSONParser();
			Object resultObject = parser.parse(json);
			JSONArray array = (JSONArray) resultObject;
			System.out.println("TOTAL NUMBER OF SENSORS: "+array.size());
			int activeSensors = 0;
			for (Object elem: array) 
			{
				JSONObject obj = (JSONObject)elem;
				String id = (String) obj.get("id");
				String status = (String) obj.get("status");
				String stype = (String) obj.get("type");
				long createdAt =  (Long) obj.get("createdAt");

				if(status.equalsIgnoreCase("ACTIVE"))
				{
					activeSensors ++;
				}

				System.out.println("ID :: "+id +", STATUS :: "+status +", TAG TYPE , "+stype+", CREATED AT - "+createdAt);
			}

			int inactiveAssets = array.size()-activeSensors;
			System.out.println("TOTAL ACTIVE TAGGED ASSETS : "+activeSensors);
			System.out.println("TOTAL INACTIVE TAGGED ASSETS : "+inactiveAssets);


			customerDataList.add("TOTAL SENSORS: "+array.size());

			customerDataList.add("TOTAL ACTIVE SENSORS: "+activeSensors);

			customerDataList.add("TOTAL INACTIVE SENSORS: "+inactiveAssets);

		} catch (Exception e) {
			e.printStackTrace();
		}


	}

	public static void getAssetCategories(String token,String clusterName)
	{

		String getUsersUrl = "https://"+clusterName+ ".cloudleaf.io/cloudos/api/1/customer/categories";

		try{
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			HttpGet getRequest = new HttpGet(getUsersUrl);
			getRequest.addHeader("content-type", "application/json");
			getRequest.addHeader("token", token);
			HttpResponse result = httpClient.execute(getRequest);
			String json = EntityUtils.toString(result.getEntity(), "UTF-8");
			System.out.println(result.getStatusLine()+"\n "+json);
			JSONParser parser = new JSONParser();
			Object resultObject = parser.parse(json);
			JSONArray array = (JSONArray) resultObject;
			int totalCategories = array.size();
			System.out.println("TOTAL NUMBER OF CATEGORIES: "+totalCategories);

			customerDataList.add("TOTAL CATEGORIES: "+totalCategories);

			for (Object elem: array) 
			{
				JSONObject obj = (JSONObject)elem;
				String name = (String) obj.get("name");			
				System.out.println("NAME :: "+name);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void getProvisionedReceivers(String token,String clusterName)
	{

		String getUsersUrl = "https://"+clusterName+ ".cloudleaf.io/cloudos/api/1/receiver/all/PROVISIONED";
		int mccCount = 0;
		int gwCount = 0;
		int lmCount = 0;

		try{
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			HttpGet getRequest = new HttpGet(getUsersUrl);
			getRequest.addHeader("content-type", "application/json");
			getRequest.addHeader("token", token);

			HttpResponse result = httpClient.execute(getRequest);
			String json = EntityUtils.toString(result.getEntity(), "UTF-8");
			System.out.println(result.getStatusLine()+"\n "+json);

			JSONParser parser = new JSONParser();
			Object resultObject = parser.parse(json);
			JSONArray array = (JSONArray) resultObject;
			int totalProvisionedReceivers = array.size()-1; //The -1 is to exclued the Out Of Coverage receiver
			System.out.println("TOTAL NUMBER OF PROVISIONED Receivers: "+totalProvisionedReceivers);

			customerDataList.add("TOTAL PROVISIONED RECEIVERS: "+totalProvisionedReceivers);

			for (Object elem: array) 
			{
				JSONObject obj = (JSONObject)elem;
				String gwPhysicalId = (String) obj.get("physicalId");
				String gwType = (String) obj.get("type");
				System.out.println("GW PHYSICAL ID :: "+gwPhysicalId);
				System.out.println("GATEWAY TYPE :: "+gwType);

				if(gwType.equalsIgnoreCase("MICRO_CLOUD_CONNECTOR"))
				{
					mccCount++;

					customerDataList.add("MCC PHYSICAL ID : "+gwPhysicalId);

					Utils.getReceiverAttributes(token, gwPhysicalId,clusterName);

				}

				else if(gwType.equalsIgnoreCase("CLOUD_CONNECTOR"))
				{
					gwCount++;

					customerDataList.add("GW PHYSICAL ID : "+gwPhysicalId);

					Utils.getReceiverAttributes(token, gwPhysicalId,clusterName);

				}

				else if(gwType.equalsIgnoreCase("LOCATION_MARKER"))
				{
					lmCount++;

					customerDataList.add("LM PHYSICAL ID : "+gwPhysicalId);

					Utils.getReceiverAttributes(token, gwPhysicalId,clusterName);

				}

			}

			System.out.println("Total Number of MCCS :: "+ mccCount);

			System.out.println("Total Number of LMS :: "+ lmCount);

			System.out.println("Total Number of GWs :: "+ gwCount);

			customerDataList.add("TOTAL PROVISIONED MCCS : "+mccCount);

			customerDataList.add("TOTAL PROVISIONED LMS : "+lmCount);

			customerDataList.add("TOTAL PROVISIONED GWS : "+gwCount);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void getProvisionedReceivers(String token,String clusterName,String version,String timestamp)
	{

		String getUsersUrl = "https://"+clusterName+ ".cloudleaf.io/cloudos/api/1/receiver/all/PROVISIONED";
		int mccCount = 0;
		int gwCount = 0;
		int lmCount = 0;

		try{
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			HttpGet getRequest = new HttpGet(getUsersUrl);
			getRequest.addHeader("content-type", "application/json");
			getRequest.addHeader("token", token);

			HttpResponse result = httpClient.execute(getRequest);
			String json = EntityUtils.toString(result.getEntity(), "UTF-8");
			System.out.println(result.getStatusLine()+"\n "+json);

			JSONParser parser = new JSONParser();
			Object resultObject = parser.parse(json);
			JSONArray array = (JSONArray) resultObject;
			int totalProvisionedReceivers = array.size()-1; //The -1 is to exclued the Out Of Coverage receiver
			System.out.println("TOTAL NUMBER OF PROVISIONED Receivers: "+totalProvisionedReceivers);

			customerDataList.add("TOTAL PROVISIONED RECEIVERS: "+totalProvisionedReceivers);

			for (Object elem: array) 
			{
				JSONObject obj = (JSONObject)elem;
				String gwPhysicalId = (String) obj.get("physicalId");
				String gwType = (String) obj.get("type");
				System.out.println("GW PHYSICAL ID :: "+gwPhysicalId);
				System.out.println("GATEWAY TYPE :: "+gwType);

				if(gwType.equalsIgnoreCase("MICRO_CLOUD_CONNECTOR"))
				{
					mccCount++;

					customerDataList.add("MCC PHYSICAL ID : "+gwPhysicalId);

					Utils.getReceiverAttributes(token, gwPhysicalId,version,timestamp,clusterName);

				}

				else if(gwType.equalsIgnoreCase("CLOUD_CONNECTOR"))
				{
					gwCount++;

					customerDataList.add("GW PHYSICAL ID : "+gwPhysicalId);

					Utils.getReceiverAttributes(token, gwPhysicalId,version,timestamp,clusterName);

				}

				else if(gwType.equalsIgnoreCase("LOCATION_MARKER"))
				{
					lmCount++;

					customerDataList.add("LM PHYSICAL ID : "+gwPhysicalId);

					Utils.getReceiverAttributes(token, gwPhysicalId,clusterName);
					//Utils.getReceiverAttributes(token, gwPhysicalId,version,timestamp);

				}

			}

			System.out.println("Total Number of MCCS :: "+ mccCount);

			System.out.println("Total Number of LMS :: "+ lmCount);

			System.out.println("Total Number of GWs :: "+ gwCount);

			customerDataList.add("TOTAL PROVISIONED MCCS : "+mccCount);

			customerDataList.add("TOTAL PROVISIONED LMS : "+lmCount);

			customerDataList.add("TOTAL PROVISIONED GWS : "+gwCount);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void getReceiverAttributes(String token,String physicalId,String clusterName)
	{

		String getUsersUrl = "https://"+clusterName+ ".cloudleaf.io/cloudos/api/1/receiver/physicalId/"+physicalId;
		int mccCount = 0;

		try{
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			HttpGet getRequest = new HttpGet(getUsersUrl);
			getRequest.addHeader("content-type", "application/json");
			getRequest.addHeader("token", token);

			HttpResponse result = httpClient.execute(getRequest);
			String json = EntityUtils.toString(result.getEntity(), "UTF-8");
			System.out.println(result.getStatusLine()+"\n "+json);

			JSONParser parser = new JSONParser();
			Object resultObject = parser.parse(json);

			JSONObject obj = (JSONObject)resultObject;

			JSONObject attributes = (JSONObject)obj.get("attributes");
			int totalAttributes = attributes.size();
			System.out.println("TOTAL ATTRIBUTES :: "+ totalAttributes);

			System.out.println("RSSI RANGE :: "+attributes.get("rssiRange"));
			System.out.println("RSSI RANGE DISPLAY  :: "+attributes.get("rssiRangeDisplay"));
			System.out.println("RSSI RANGE VALUE  :: "+attributes.get("rssiRangeValue"));

			customerDataList.add("RSSI RANGE:  "+attributes.get("rssiRange"));

			customerDataList.add("RSSI RANGE DISPLAY:  "+attributes.get("rssiRangeDisplay"));

			customerDataList.add("RSSI RANGE VALUE:  "+attributes.get("rssiRangeValue"));

			customerDataList.add("TIME STAMP:  "+attributes.get("timestamp"));

			customerDataList.add("VERSION:  "+attributes.get("version"));




		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void getReceiverAttributes(String token,String physicalId,String version,String timestamp,String clusterName)
	{

		String getUsersUrl = "https://"+clusterName+ ".cloudleaf.io/cloudos/api/1/receiver/physicalId/"+physicalId;
		StringBuilder receiverTimeStamp = new StringBuilder();
		StringBuilder versionValidator = new StringBuilder();


		try{
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			HttpGet getRequest = new HttpGet(getUsersUrl);
			getRequest.addHeader("content-type", "application/json");
			getRequest.addHeader("token", token);

			HttpResponse result = httpClient.execute(getRequest);
			String json = EntityUtils.toString(result.getEntity(), "UTF-8");
			System.out.println(result.getStatusLine()+"\n "+json);

			JSONParser parser = new JSONParser();
			Object resultObject = parser.parse(json);

			JSONObject obj = (JSONObject)resultObject;

			JSONObject attributes = (JSONObject)obj.get("attributes");
			int totalAttributes = attributes.size();
			System.out.println("TOTAL ATTRIBUTES :: "+ totalAttributes);

			System.out.println("RSSI RANGE :: "+attributes.get("rssiRange"));
			System.out.println("RSSI RANGE DISPLAY  :: "+attributes.get("rssiRangeDisplay"));
			System.out.println("RSSI RANGE VALUE  :: "+attributes.get("rssiRangeValue"));

			customerDataList.add("RSSI RANGE:  "+attributes.get("rssiRange"));

			customerDataList.add("RSSI RANGE DISPLAY:  "+attributes.get("rssiRangeDisplay"));

			customerDataList.add("RSSI RANGE VALUE:  "+attributes.get("rssiRangeValue"));

			customerDataList.add("TIME STAMP:  "+attributes.get("timestamp"));

			customerDataList.add("VERSION:  "+attributes.get("version"));

			//Validate the time stamp with actual TIME stamp which is input through FCT Tool

			if(attributes.get("timestamp") != null && String.valueOf(attributes.get("timestamp")).equals(timestamp))
			{
				System.out.println("TIME STAMP Matches for the GW "+ physicalId);
				//receiverTimeStamp.append("TIME STAMP  matched for the GW "+physicalId  + " And the TIMESTAMP is :"+attributes.get("timestamp") + "\r\n");
			}

			else
			{
				System.out.println("TIME STAMP didn't match for the GW "+physicalId  + " Actual TIMESTAMP is :"+attributes.get("timestamp") + "  But Expecteced TIMESTAMP is :"+timestamp);
				receiverTimeStamp.append("TIME STAMP didn't match for the GW "+physicalId  + " Actual TIMESTAMP is :"+attributes.get("timestamp") + "  But Expecteced TIMESTAMP is :"+timestamp + "\r\n");
			}

			customerDataList.add(receiverTimeStamp.toString());

			if(attributes.get("version") != null && String.valueOf(attributes.get("version")).equals(version))
			{
				System.out.println("VERSION Matches for the GW "+physicalId);
				//versionValidator.append("VERSION MAtched for the GW "+physicalId  + " And the VERSION is :"+attributes.get("version") + "\r\n");
			}

			else
			{
				System.out.println("VERSION didn't match for the GW "+physicalId  + " And the VERSION is :"+attributes.get("version"));
				versionValidator.append("VERSION didn't match for the GW "+physicalId  + " Actaul VERSION is :"+attributes.get("version") + "  But Expecteced Version is :"+version +"\r\n");
			}

			customerDataList.add(versionValidator.toString());


		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getSites(String token,String clusterName)
	{

		String getUsersUrl = "https://"+clusterName+ ".cloudleaf.io/cloudos/api/1/customer?depth=2";
		String tenantId = null;

		try{
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			HttpGet getRequest = new HttpGet(getUsersUrl);
			getRequest.addHeader("content-type", "application/json");
			getRequest.addHeader("token", token);
			HttpResponse result = httpClient.execute(getRequest);
			String json = EntityUtils.toString(result.getEntity(), "UTF-8");
			System.out.println(result.getStatusLine()+"\n "+json);

			JSONParser parser = new JSONParser();
			Object resultObject = parser.parse(json);

			JSONObject obj = (JSONObject)resultObject;
			String tenantName = (String) obj.get("name");	

			System.out.println("TENANT NAME:: "+ tenantName);

			tenantId = (String) obj.get("identifier");

			System.out.println("TENANT ID:: "+ tenantId);

			JSONArray sites = (JSONArray) obj.get("sites");
			int totalSites = sites.size()-1;
			System.out.println("TOTAL SITES :: "+ totalSites);

			customerDataList.add("TENANT NAME: "+tenantName);

			customerDataList.add("TENANT ID: "+tenantId);

			customerDataList.add("TOTAL SITES: "+totalSites);

			for(Object sitename: sites)
			{
				JSONObject siteobj = (JSONObject)sitename;
				System.out.println("SITE Name :"+siteobj.get("name"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return tenantId;
	}

	public static String fillSitesAndGetTenant(String token,String clusterName)
	{
		String tenantId = null;

		String getUsersUrl = "https://"+clusterName+ ".cloudleaf.io/cloudos/api/1/customer?depth=2";

		try{
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			HttpGet getRequest = new HttpGet(getUsersUrl);
			getRequest.addHeader("content-type", "application/json");
			getRequest.addHeader("token", token);
			HttpResponse result = httpClient.execute(getRequest);
			String json = EntityUtils.toString(result.getEntity(), "UTF-8");
			System.out.println(result.getStatusLine()+"\n "+json);

			JSONParser parser = new JSONParser();
			Object resultObject = parser.parse(json);

			JSONObject obj = (JSONObject)resultObject;
			String tenantName = (String) obj.get("name");	

			System.out.println("TENANT NAME:: "+ tenantName);

			tenantId = (String) obj.get("identifier");

			System.out.println("TENANT ID:: "+ tenantId);

			JSONArray sites = (JSONArray) obj.get("sites");
			int totalSites = sites.size();
			System.out.println("TOTAL SITES :: "+ sites.size());

			customerDataList.add("TENANT NAME: "+tenantName);

			customerDataList.add("TENANT ID: "+tenantId);

			customerDataList.add("TOTAL SITES: "+totalSites);

			for(Object sitename: sites)
			{
				JSONObject siteobj = (JSONObject)sitename;
				System.out.println("SITE Name :"+siteobj.get("name"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return tenantId;
	}

	// Caesar cipher
	static String encrypt(String text, int offset)
	{
		char[] plain = text.toCharArray();

		for (int i = 0; i < plain.length; i++) {
			for (int j = 0; j < chars.length; j++) {
				if (j <= chars.length - offset) {
					if (plain[i] == chars[j]) {
						plain[i] = chars[j + offset];
						break;
					}
				} 
				else if (plain[i] == chars[j]) {
					plain[i] = chars[j - (chars.length - offset + 1)];
				}
			}
		}
		return String.valueOf(plain);
	}

	static String get(String cip)
	{
		char[] cipher = cip.toCharArray();
		for (int i = 0; i < cipher.length; i++) {
			for (int j = 0; j < chars.length; j++) {
				if (j >= 6 && cipher[i] == chars[j]) {
					cipher[i] = chars[j - 6];
					break;
				}
				if (cipher[i] == chars[j] && j < 6) {
					cipher[i] = chars[(chars.length - 6 +1) + j];
					break;
				}
			}
		}
		return String.valueOf(cipher);
	}
}