package com.cloudleaf.restapicalls;


import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class BramblesData {


	static String tenantId = null;
	static final String USER = "sridhar.bathina@cloudleaf.io";
	static final String PASSWORD = "Sridhar57#";
	static final String EPASSWORD = "YxojngxBD)";
	//static final String TENANTID= "bedd978f-7673-487a-905f-39bf42b7dfce";
	public String tenantName = null;
	public String clusterName = null;
	
	@BeforeClass
	public void classSetup()
	{
		clusterName = System.getProperty("clusterName");
	}

	@Test
	public void BramblesData()
	{
		String currentDir = System.getProperty("user.dir");
	    System.out.println("Current dir using System:" +currentDir);
	    
		String token1=null;
		//token1=Utils.getToken(USER,PASSWORD,clusterName);
		token1=Utils.getToken(USER,new EncryptionExample().decrypt(EPASSWORD, 6),clusterName);
		
		//Gives the Tenant Name and Total number of Sites in the Tenant
		tenantId=Utils.getSites(token1,clusterName);

		//Gives the Total number of Users  and Total number of Provisioned Gateways in the Tenant
		Utils.getUsers(token1,tenantId,clusterName);

		//Gives the Total number of Sensors,Active and Inactive sensors in the Tenant
		Utils.getSensors(token1, tenantId,clusterName);

	//Gives the Total number of Asse,clusterNamet Categories in the Tenant
		Utils.getAssetCategories(token1,clusterName);
		
		//Utils.getProvisionedReceivers(token1,clusterName);
		
		Utils.getProvisionedReceivers(token1,clusterName,"1.1.0","1528357765");

		//Create file Utils.txt file in D Drive
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(currentDir+"\\BramblesData.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		////Write the Customer Data which is stored in the ArrayList to the above file
		for(Object obj:Utils.customerDataList)
		{
			out.println(obj);
		}

		out.close();

		//Clearing the array list
		Utils.customerDataList.clear();

	}



}
