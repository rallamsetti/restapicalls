package com.cloudleaf.restapicalls;


import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class OrangeData {

	static String tenantId = null;
	static final String USER = "CLFOrangeSVAdmin";
	static final String PASSWORD = "IRLUxgtmkY@Gjsot789)";
	//static final String TENANTID= "87e5b28d-6b34-4a98-9f09-2c48a3169611";
	String clusterName = null;

	@BeforeClass
	public void classSetup()
	{
		clusterName = System.getProperty("clusterName");
	}

	//public static void main(String[] args) throws FileNotFoundException
	@Test
	public void orangeData() 
	{

		String currentDir = System.getProperty("user.dir");
	    System.out.println("Current dir using System:" +currentDir);
	    
		String token1=null;
		//token1=Utils.getToken(USER,PASSWORD,clusterName);
		token1=Utils.getToken(USER,Utils.get(PASSWORD),clusterName);
		
		//Gives the Tenant Name and Total number of Sites in the Tenant
		tenantId=Utils.getSites(token1,clusterName);

		//Gives the Total number of Users  and Total number of Provisioned Gateways in the Tenant
		Utils.getUsers(token1,tenantId,clusterName);

		//Gives the Total number of Sensors,Active and Inactive sensors in the Tenant
		Utils.getSensors(token1, tenantId,clusterName);

	//Gives the Total number of Asse,clusterNamet Categories in the Tenant
		Utils.getAssetCategories(token1,clusterName);
		
		//Utils.getProvisionedReceivers(token1,clusterName);
		
		Utils.getProvisionedReceivers(token1,clusterName,"1.1.0","1528357765");

		//Create file OrangeData.txt file in D Drive
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(currentDir+"\\ProdData\\OrangeData.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		//Write the Customer Data which is stored in the ArrayList to the above file
		for(Object obj:Utils.customerDataList)
		{
			out.println(obj);
		}

		out.close();

		//Clearing the array list
		Utils.customerDataList.clear();

	}



}
