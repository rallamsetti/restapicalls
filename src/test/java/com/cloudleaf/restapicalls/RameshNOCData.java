package com.cloudleaf.restapicalls;


import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class RameshNOCData {

	static String token = null;
	static String tenantId = null;
	static final String USER = "rameshnoc2";
	static final String PASSWORD = "Easwar123$";
	//static final String TENANTID = "e55cbef6-c5a0-4121-94cb-263f31f68183";
	public String clusterName = null;
	@BeforeClass
	public void classSetup()
	{
		clusterName = System.getProperty("clusterName");
	}

	@Test
	public void rameshNocData()
	{

		String currentDir = System.getProperty("user.dir");
	    System.out.println("Current dir using System:" +currentDir);
	    
		String token1=null;
		token1=Utils.getToken(USER,PASSWORD,clusterName);
		
		//Gives the Tenant Name and Total number of Sites in the Tenant
		tenantId=Utils.getSites(token1,clusterName);

		//Gives the Total number of Users  and Total number of Provisioned Gateways in the Tenant
		Utils.getUsers(token1,tenantId,clusterName);

		//Gives the Total number of Sensors,Active and Inactive sensors in the Tenant
		Utils.getSensors(token1, tenantId,clusterName);

	//Gives the Total number of Asse,clusterNamet Categories in the Tenant
		Utils.getAssetCategories(token1,clusterName);
		
		//Utils.getProvisionedReceivers(token1,clusterName);
		
		Utils.getProvisionedReceivers(token1,clusterName,"1.1.0","1528357765");

		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(currentDir+"\\RameshNOCData.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		for(Object obj:Utils.customerDataList)
		{
			out.println(obj);
		}

		out.close();

		//Clearing the array list
		Utils.customerDataList.clear();

	}



}
