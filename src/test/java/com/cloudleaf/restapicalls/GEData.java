package com.cloudleaf.restapicalls;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class GEData {

	static String tenantId = null;
	static final String USER = "CLFGEAppAdmin";
	static final String PASSWORD = "IRLMKGvvGjsot789)";
	//static final String TENANTID= "6215ce8d-abf2-4ca5-b3d1-16a580b877fa";
	
	public String clusterName = null;
	
	@BeforeSuite
	public void removeAllFiles() throws IOException
	{
		System.out.println("Before Suite is executed first and it will remove all the file in ProdData Folder");
		File file = new File(System.getProperty("user.dir")+"\\ProdData");      
		String[] myFiles = null;    
		if(file.isDirectory()){
			myFiles = file.list();
			System.out.println("Number of Files in the Prod Data Directory: "+ myFiles.length);
			
			for (int i=0; i<myFiles.length; i++) {
				File myFile = new File(file, myFiles[i]); 
				myFile.delete();
			}
		}
		
		System.out.println("All Files in the ProdData are Deleted and number of files in ProdData Folder are : " + myFiles.length);
	}
	
	@BeforeClass
	public void classSetup()
	{
		clusterName = System.getProperty("clusterName");
	}
	
/*	String userid,password;
	 String suitexml;
	@BeforeClass
	public void beforeClass() throws Exception
	{
		userid = System.getProperty("userName");
		
		password = System.getProperty("password");
	}*/
	
	//public static void main(String[] args) throws FileNotFoundException
	@Test
	public void geData() 
	{

		String currentDir = System.getProperty("user.dir");
	    System.out.println("Current dir using System:" +currentDir);
	    
		String token1=null;
		//token1=Utils.getToken(USER,PASSWORD,clusterName);
		
		token1=Utils.getToken(USER,Utils.get(PASSWORD),clusterName);
		//Gives the Tenant Name and Total number of Sites in the Tenant
		tenantId=Utils.getSites(token1,clusterName);

		//Gives the Total number of Users  and Total number of Provisioned Gateways in the Tenant
		Utils.getUsers(token1,tenantId,clusterName);

		//Gives the Total number of Sensors,Active and Inactive sensors in the Tenant
		Utils.getSensors(token1, tenantId,clusterName);

	//Gives the Total number of Asse,clusterNamet Categories in the Tenant
		Utils.getAssetCategories(token1,clusterName);
		
		//Utils.getProvisionedReceivers(token1,clusterName);
		
		Utils.getProvisionedReceivers(token1,clusterName,"1.1.0","1528357765");
		
		//Utils.getAssets(token1, TENANTID);

	   //Create file GEData.txt file in D Drive
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(currentDir+"\\ProdData\\GEData.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//Write the Customer Data which is stored in the ArrayList to the above file
		for(Object obj:Utils.customerDataList)
		{
			out.println(obj);
		}

		out.close();
		
		//Clearing the array list
		Utils.customerDataList.clear();
		
		
	}
	
}
	
